import React from "react";
import { useMediaQuery } from "react-responsive";
import BCarousel from "react-bootstrap/Carousel";
import Container from "react-bootstrap/Container";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import Image from "react-bootstrap/Image";

import HeroContainer from "./styles";
import HeroBanner from "../../assets/home/banner.jpg";
import Logo from "../../assets/aguagente-logo.png";

const Carrousel = (props) => {
  const midSize = useMediaQuery({ query: "(max-width: 950px)" });
  const images = [{ src: HeroBanner, id: 0 }];

  const renderImages = images.map((image) => (
    <BCarousel.Item
      className="carousel__item carousel__size"
      style={{ position: "relative" }}
    >
      <Container
        fluid
        className="p-0 d-flex flex-column align-items-center h-100"
      >
        <img
          className="d-block w-100 position-absolute carousel__size"
          src={image.src}
          alt="First slide"
        />
        <Row className="w-100 h-100">
          <Container fluid className="overlay">
            <Container className="carousel__content h-100">
              <Row>
                <Col md={4} />
                <Col sm={12} md={8} className="text-center text-white h-100">
                  <h2
                    className={`display-3 text-white mb-3 mt-5 pt-5 text-center heroTitle`}
                    style={{ fontWeight: 900 }}
                  >
                    Sistema de
                    <br /> Purificación de agua
                  </h2>
                  <p className="h5">
                    Aguagente proporciona agua purificada ilimitada a tu familia
                    mediante la instalación de nuestro equipo avanzado de
                    purificación en tu hogar por una tarifa de $349 mensuales.
                  </p>
                  <button
                    type="button"
                    className="custom-button__green mr-auto ml-auto mt-3 p-2 h5"
                  >
                    Comienza a disfrutar
                  </button>
                  <Image src={Logo} className="hero__logo" />
                </Col>
              </Row>
            </Container>
          </Container>
        </Row>
      </Container>
    </BCarousel.Item>
  ));
  return (
    <HeroContainer fluid className="carousel__size px-0" interval={10000}>
      <BCarousel
        controls={false}
        indicators={!midSize}
        className="h-100 carousel__size"
      >
        {renderImages}
      </BCarousel>
    </HeroContainer>
  );
};

export default Carrousel;
