import Container from "react-bootstrap/Container";
import styled from "styled-components";

const HeroContainer = styled(Container)`
  .fullHeight {
    height: 100vh;
  }

  .mobileHeight {
    height: 40vh;
  }

  .heroTitle {
    font-size: 50px;
  }

  .carousel__item {
    height: 100vh;
  }

  .hero__logo {
    height: 300px;
    width: 300px;
    margin-top: 20px;
  }

  @media (max-width: 950px) {
    .overlay {
      background: rgba(0, 0, 0, 0.3);
      z-index: 1;
    }
    .carousel__size {
      height: 40vh !important;
      position: relative;
    }
  }

  @media (max-width: 1026px) {
    .carousel__size {
      height: 50vh !important;
    }
    .heroTitle {
      margin-top: 10px !important;
      margin-bottom: 10px !important;
      font-size: 24px !important;
    }
    .hero__logo {
      height: 200px;
      width: 200px;
      margin-top: 5px;
    }
    p {
      font-size: 16px !important;
    }
    .custom-button__green {
      font-size: 16px !important;
    }
  }
  @media (max-width: 378px) {
    .hero__logo {
      height: 100px;
      width: 100px;
      margin-top: 0;
    }
  }
`;

export default HeroContainer;
