import React from 'react';
import SocialMediaContainer from './styles';
import Image from 'react-bootstrap/Image';

import facebook from '../../assets/socialMedia/facebook.png';
import youtube from '../../assets/socialMedia/youtube.png';
import likeIcon from '../../assets/socialMedia/like-icon.png';
import instagram from '../../assets/socialMedia/instagram.png';
import twitter from '../../assets/socialMedia/twitter.png';

const SocialMediaGroup = (props) => (
  <SocialMediaContainer { ...props } className="d-flex justify-content-center social-media my-4">
    <a href="#" className="social-media__icon mr-2"><Image src={facebook} /></a>
    <a href="#" className="social-media__icon mr-2"><Image src={instagram} /></a>
    <a href="#" className="social-media__icon mr-2"><Image src={twitter} /></a>
    <a href="#" className="social-media__icon mr-2"><Image src={youtube} /></a>
    <a href="#" className="social-media__icon "><Image src={likeIcon} /></a>
  </SocialMediaContainer>
);

export default SocialMediaGroup;
