import StyledButton from "./styles";

const Button = (props) => {
  const { handleClick, children, type } = props;
  return (
    <StyledButton type={type || "button"} onClick={handleClick}>
      {children}
    </StyledButton>
  );
};

export default Button;
