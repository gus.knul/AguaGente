import styled from "styled-components";
import primaryColor from "../../PageStyles/globalStyles";

const StyledButton = styled.button`
  border: none;
  outline: none;
  padding: 1rem;
  color: white;
  background-color: ${primaryColor};
  border-radius: 0.7rem;
  transition: all 0.3s ease;
  &:hover {
      cursor: pointer;
      transform: translateY(-0.3rem);
      box-shadow: 0px 10px 18px -10px rgba(0,0,0,0.72);
  &:active {
      transform: translateY(0.1rem);
  }
`;

export default StyledButton;
