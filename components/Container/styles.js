import styled, { css } from "styled-components";
import primaryColor, { midBreakpoint } from "../../PageStyles/globalStyles";

const itemStyles = css`
  display: flex;
  margin-left: 2rem;
  align-items: center;
  justify-content: center;
  & a {
    transition: all 0.4s ease;
    padding: 0.5rem;
    text-decoration: none;
    color: white;
    &:after {
    }
    &:hover {
      cursor: pointer;
      &:after {
        width: 100%;
        background: white;
      }
    }
  }
  & input {
    width: 100%;
  }
  & button {
    border: none;
    outline: none;
    border-radius: 50%;
    width: 2rem;
    height: 2rem;
    bacground-color: red;
    &:hover {
      cursor: pointer;
    }
  }
`;

const NavContainer = styled.nav`
  display: flex;
  width: 100%;
  height: 8rem;
  font-size: 2rem;
  z-index: 1;
  transition: all 0.2s ease;
  background-color: transparent;
  background: rgb(2, 0, 36);
  background: linear-gradient(
    180deg,
    rgba(2, 0, 36, 0.8) 0%,
    rgba(0, 0, 0, 0) 100%
  );

  @media (max-width: ${midBreakpoint}) {
    flex-direction: column;
  }
  & .hamburger {
    border: none;
    outline: none;
  }
  &.sticky {
    position: fixed;
    top: 0;
    left: 0;
    animation: moveDown 0.5s ease-in-out;
  }
  .nav {
    &__content {
      display: flex;
      width: 100%;
      align-items: center;
    }
    &__brand {
      margin-left: 1rem;
      flex: 2;
    }
    &__items {
      @media (max-width: ${midBreakpoint}) {
        display: none;
      }
      display: flex;
      flex: 8;
      justify-content: center;
      margin-right: 10rem;
      & .item {
        ${itemStyles}
      }
      &.mobile {
        display: none;
        @media (max-width: ${midBreakpoint}) {
          display: flex;
          margin-right: 1rem;
        }
      }
    }
  }

  .mobile__menu {
    margin-left: 1.5rem;
    & .item {
      ${itemStyles}
      margin-left: 0;
      justify-content: flex-start;
      margin-bottom: 1rem;
      & a {
        width: 12rem;
      }
    }
  }

  @keyframes moveDown {
    from {
      transform: translateY(-5rem);
    }
    to {
      transform: translateY(0);
    }
  }
`;

const ContentArea = styled.div`
  width: 100%;
  margin-top: -67px;
`;

export { NavContainer as default, ContentArea };
