import Link from "next/link";
import { ContentArea } from "./styles";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";

import styles from "./index.module.css";

const Container = ({ children }) => {
  const navItems = [
    { text: "Inicio", path: "/" },
    { text: "Programa de Referidos", path: "/referrals" },
    { text: "Nosotros", path: "/us" },
    { text: "Inversionistas", path: "/investors" },
    { text: "Distribución", path: "/distribution" },
    { text: "FAQs", path: "/faqs" },
  ];
  const renderNavItems = navItems.map(({ text, path }, idx) => (
    <div className={`item `} key={idx}>
      <Link href={path} passHref>
        <Nav.Link className={`text-white pr-3 ${styles.white_animation}`}>
          <b>{text}</b>
        </Nav.Link>
      </Link>
    </div>
  ));
  return (
    <>
      <Navbar
        expand="md"
        sticky="top"
        variant="dark"
        className={`${styles.faded_bg} py-3 text-white`}
      >
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto ml-auto">{renderNavItems}</Nav>
        </Navbar.Collapse>
      </Navbar>
      <ContentArea>{children}</ContentArea>
    </>
  );
};

export default Container;
