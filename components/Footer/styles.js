import Container from 'react-bootstrap/Container';
import styled from 'styled-components';

const FooterContainer = styled(Container)`
      margin-top: 40px;
      padding: 25px 0;
      color: #5a6464;
      background-color: #04dbf4;
      text-align: center;
      & p {
        font-weight: 500;
      }
`;

export default FooterContainer;
