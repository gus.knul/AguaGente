import styled from "styled-components";

const BracketContainer = styled.div`
  padding: 1rem 3rem;
  background-image: linear-gradient(#4a4a4a, #4a4a4a),
    linear-gradient(#4a4a4a, #4a4a4a), linear-gradient(#4a4a4a, #4a4a4a),
    linear-gradient(#4a4a4a, #4a4a4a);

  background-repeat: no-repeat;
  background-size: 30px 5px;
  // ^^^ This value should be equal to width of left OR right border.
  background-position: top left, top right, bottom left, bottom right;

  border: solid black;
  border-width: 0 5px;
`;

export default BracketContainer;
