import Container from "react-bootstrap/Container";
import styled from "styled-components";

const WarningContainer = styled(Container)`
  .warning-banner {
    background-color: #ffa358 !important;
  }

  @media (max-width: 950px) {
    p {
      font-size: 16px !important;
    }
  }
`;

export default WarningContainer;
