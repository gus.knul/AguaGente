import React from 'react';
import Link from 'next/link';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import BCarousel from 'react-bootstrap/Carousel';
import Image from 'react-bootstrap/Image';
import Form from 'react-bootstrap/Form';
import { Player, BigPlayButton } from 'video-react';

import HomeContent from '../PageStyles/homeStyles';
import testimonial1 from '../assets/home/testimonials/testimonios-05.jpg';
import testimonial2 from '../assets/home/testimonials/testimonios-01.jpg';
import testimonial3 from '../assets/home/testimonials/testimonios-03.jpg';
import testimonial4 from '../assets/home/testimonials/testimonios-04.jpg';
import formIcon from '../assets/home/icono-formulario.png';

import { Carrousel, SocialMediaGroup } from '../components';
import CuerpoSanoImg from '../assets/home/cuerpo-sano.png';
import CovidFamilyImage from '../assets/home/acciones-covid-familia.jpg';

const Index = () => {
  const PrevIcon = <span className="display-3 text-dark">&#8249;</span>;
  const NextIcon = <span className="display-3 text-dark">&#8250;</span>;

  const comments = [
    {
      id: 1,
      src: testimonial1,
      text: '"Estoy muy tranquila de que mis hijos toman agua de excelenete calidad"',
      name: 'Adriana Bernádez',
    },
    {
      id: 2,
      src: testimonial2,
      text: '"Felíz por su sabor, y saber que estoy tomando agua din contaminantes"',
      name: 'Alma Zambrano',
    },
    {
      id: 3,
      src: testimonial3,
      text:
        '"Por ser muy conveniente, además de contribuir con empresas mexicanas socialmente responsables"',
      name: 'Miriam Martínez',
    },
    {
      id: 4,
      src: testimonial4,
      text: '"He invitado a varias de mis amistades y ahora tengo agua gratis"',
      name: 'Leticia Ambrosio',
    },
  ];

  const renderComments = comments.map((comment) => (
    <div className="px-3" key={comment.id}>
      <Image roundedCircle src={comment.src} className="commentImage" />
      <p className="mt-3">{comment.text}</p>
      <p>{comment.name}</p>
    </div>
  ));

  return (
    <HomeContent>
      <Carrousel />
      <Container fluid className="registration-add__section px-0 mt-4">
        <Container>
          <Row className="registration-add">
            <Col
              sm={12}
              md={8}
              className={`
            d-flex 
            flex-column 
            justify-content-center 
            align-items-center
            align-items-lg-start
            px-0
            px-sm-2
            `}
            >
              <h1>
                <span>Agua purificada</span>
                {' '}
                para tu familia
              </h1>
              <div className="registration-add__info">
                <p className="mb-0 ">
                  de
                  {' '}
                  <span>349</span>
                  {' '}
                  a
                </p>
                <div className="registration-add__info__promotion-price">
                  <span>$</span>
                  <span>299</span>
                </div>
                <p className="pb-5">mensuales</p>
              </div>
              <div className="registration-add__action mb-4">
                <button type="button" className="custom-button__green px-5 py-4 h1 ml-5 mb-5">
                  <b className="mr-3">Registrate</b>
                  Aquí
                </button>
              </div>
            </Col>
          </Row>
        </Container>
      </Container>
      <Container as="section" fluid className="know-us">
        <Container className="h-100 main__container">
          <Row className="know-us__container">
            <Col md={6} sm={12} lg={8} xs={12} className="video-player">
              <Player>
                <source
                  src="https://onedrive.live.com/download?cid=3619C363C6382B38&resid=3619C363C6382B38%2139937&authkey=AB5ke63w5JUmFkw"
                  type="video/mp4"
                />
                <BigPlayButton position="center" />
              </Player>
            </Col>
            <Col
              md={6}
              sm={12}
              lg={4}
              xs={12}
              className="action__content d-flex align-items-center text-center"
            >
              <p>
                <b>Conoce más </b>
                sobre el sistema
              </p>
              <p>de purificación y lo que hacemos</p>
              <Link href={{ pathname: '/us', query: { gallery: '1' } }}>
                <a className="custom-button__red py-3 px-3 h2">
                  <b>Ir a Galería </b>
                </a>
              </Link>
            </Col>
          </Row>
        </Container>
      </Container>
      <Container fluid as="section" className="content-description">
        <Container>
          <Row className="content-description__container">
            <Col sm={12} md={6} lg={7} className="image__banner">
              <img fluid alt="cuerpo sano" src={CuerpoSanoImg} className="d-xl-none" />
              <div className="image__banner__action">
                <p>Para disfrutar los beneficios</p>
                <p>Comunicate con un asesor</p>
                <button
                  type="button"
                  className="custom-button__blue text-dark h2 py-3 px-4 font-weight-bold"
                >
                  Contactar
                </button>
              </div>
            </Col>
            <Col sm={12} md={6} lg={5}>
              <Container fluid as="article" className="text-center text-md-right">
                <h2 className="text-center display-5 pt-4 mb-4">
                  <b>Con Aguagente</b>
                  {' '}
                  es
                  <br />
                  fácil tener una
                  {' '}
                  <br />
                  <b>vida más saludable</b>
                </h2>
                <p className="paragraph ">
                  <b className="p__title">Agua fresca y purificada con la máxima calidad</b>
                  <p>
                    Gracias a nuestro sistema de purificación disfruta de agua purificada en todo
                    momento gracias a la luz uv que contiene el sistema de purificación de
                    Aguagente, funciona como un bactericida al instante a diferencia de el agua
                    embotellada que crea sedimentos y tiene fecha de caducidad.
                  </p>
                </p>
                <p className="paragraph ">
                  <b className="p__title">Conveniencia</b>
                  <p>
                    Disfruta de los beneficios de contar con un sistema de purificación de agua en
                    tu hogar, ahorra espacio en tu casa, evita cargar los pesados garrafones, salir
                    de tu casa innecesariamente y disfruta de
                    {' '}
                    <b className="text-primary">
                      agua purificada las 24 horas, 7 días de la semana.
                    </b>
                  </p>
                </p>
                <p className="paragraph">
                  <b className="p__title">instalación profesional</b>
                  <p>
                    Contamos con un equipo altamente calificado y experimentado en sistemas de
                    filtración de agua el cual le brindara el mejor servicio.
                  </p>
                </p>
                <p className="paragraph">
                  <b className="p__title">Mantenimiento sin costo</b>
                  <p>
                    Una de las ventajas de contar con nuestro modelo de financiamiento es que
                    contará con asistencia técnica y Mantenimiento sin costo extra.
                  </p>
                </p>
                <p className="paragraph ">
                  <b className="p__title">Aporte a tu economía</b>
                  <p>
                    Una parte significativa del presupuesto familiar está destinado a la compra de
                    garrafones; sin embargo con un purificador de agua para casa, evitas la compra
                    de agua embotellada, en un plazo no muy lejano se verá reflejado el ahorro en
                    pro de tu economía.
                  </p>
                </p>
                <p className="paragraph ">
                  <b className="p__title">Contribuye al cuidado del medio ambiente</b>
                  <p>
                    Este accesorio para tu hogar disminuye considerable mente el uso de las botellas
                    de plástico. Estas botellas no son biodegradables en su mayoría y tardan en
                    eliminarse del ambiente mas de 400 años, por esta razón es importante adquirir
                    tu sistema de purificación de agua.
                  </p>
                </p>
              </Container>
            </Col>
          </Row>
        </Container>
      </Container>
      <Container as="section" fluid className="py-5 we-work">
        <Container>
          <h2 className="text-center h1">
            <b>Trabajamos</b>
            {' '}
            bajo los mas altos
            <br />
            {' '}
            estándares de
            <b>protocolos de salubridad</b>
          </h2>
        </Container>
      </Container>
      <Container as="section" fluid className="covid__section position-relative py-3">
        <Row>
          <Col sm={12} lg={7} className="first__col">
            <h2 className="text-center covid__title">
              ACCIONES
              {' '}
              <b>COVID-19</b>
            </h2>
            <Container className="h4 font-weight-normal">
              <p className="text-justify pt-4">
                Es por eso que hemos implementado procedimientos para garantizar la seguridad de
                nuestro personal y cliente. Preparamos equipos y filtros para la instalación para
                que la mayoría de los casos la instalación se complete en menos de una hora y los
                cambios de filtro en menos de 30 minutos.
              </p>
              <p className="text-justify pt-3">
                Nuestros técnicos cuentan con equipos de seguridad, incluidas mascaras, que usan en
                todo momento. No necesitan quedarse con nuestro técnico mientras el completa su
                instalación o cambio de filtro. Simplemente llévelo a su cocina y manténgase a una
                distancia segura de el si desea observar su trabajo.
              </p>
              <p className="text-justify pt-3">
                <b>
                  Nuestro negocio es considerado esencial por el gobierno y tenemos permiso para
                  continuar trabajando.
                </b>
              </p>
              <p className="text-justify pt-3">
                El beneficio de nuestro servicio en comparación con los garrafones es enorme,
                particularmente en este momento. Ademas de la conveniencia de agua purificada
                ilimitada por un pago mensual de bajo costo con nuestro servicio, solo necesitamos
                visitarlo una vez por menos de una hora para instalar nuestro equipo y luego puede
                comenzar a disfrutar de agua purificada ilimitada sin necesidad de recibir
                repartidores o irse a las tiendas.
              </p>
            </Container>
          </Col>
          <Col sm={12} md={6} lg={5} className="second__col text-center mx-auto mx-lg-0">
            <Image src={CovidFamilyImage} className="mt-3" />
            <p className="pt-4" style={{ color: '#2fd627' }}>
              Sabemos que este es un momento difícil para todos y
              {' '}
              <br />
              {' '}
              todos necesitamos agua
              purificada segura para
              {' '}
              <br />
              {' '}
              nuestras familias en nuestros hogares
            </p>
          </Col>
        </Row>
      </Container>
      <Container
        fluid
        className="wellness d-flex flex-column flex-md-row justify-content-center justify-content-md-end align-items-center py-3"
      >
        <p className="mb-0 h3">
          <b>Tu Bienestar </b>
          {' '}
          es primero
        </p>
        <Link href="/test">
          <a
            href="#"
            className="custom-button__blue mr-5 ml-3 mt-2 py-3 px-4 mb-1 text-dark text-decoration-none"
          >
            <b className="h3">Contacta un Asesor</b>
          </a>
        </Link>
      </Container>
      <Container as="section" fluid className="testimonials">
        <p className="testimonials__title text-center">
          <b>Cada vez somos más</b>
          {' '}
          cambiando a México
        </p>
        <BCarousel prevIcon={PrevIcon} nextIcon={NextIcon} className="carousel__body">
          <BCarousel.Item className="py-5">
            <Container className="text-center d-flex flex-column flex-lg-row justify-content-around">
              {renderComments}
            </Container>
          </BCarousel.Item>
        </BCarousel>
      </Container>
      <Container as="section" fluid className="contact__form mt-4">
        <Container className="text-center">
          <h2 className="display-3 font-weight-normal want-water">
            Quiero
            {' '}
            <b>Agua Ilimitada</b>
          </h2>
          <p className="h2">
            Contáctanos para disfrutar los beneficios de tener
            {' '}
            <br />
            {' '}
            agua purificada ilimitada en
            tu hogar.
          </p>
          <Container fluid className="d-flex flex-column align-items-center">
            <img alt="form icon" src={formIcon} className="user-icon mt-5" />
            <Form className="mt-4">
              <Row fluid>
                <Col md={6} className="form__input">
                  <Form.Control placeholder="Nombre" />
                </Col>
                <Col md={6} className="form__input">
                  <Form.Control className="mt-4 mt-md-0" placeholder="Apellido" />
                </Col>
              </Row>
              <Form.Control className="mt-4" placeholder="Teléfono" />
              <Form.Control className="mt-4" placeholder="Correo" />
              <Form.Control className="mt-4" placeholder="Comentarios" />
              <button type="submit" className="custom-button__green mt-4 h4 py-3 px-5 mx-auto">
                Enviar
              </button>
            </Form>
            <p className="mt-4 h3 font-weight-normal">
              Quieres contarle a alguien sobre Aguagente?
              <br />
              Puedes compartir esta información
              <br />
              <b>haciendo click aquí</b>
            </p>
            <SocialMediaGroup />
          </Container>
        </Container>
      </Container>
    </HomeContent>
  );
};

export default Index;
