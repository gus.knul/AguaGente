
## Description

This repo is for Aguagente project which is built in react and next.js

## Instructions

- git clone this repo
- npm install or yarn install 

## Notes

- for running it locally run yarn dev or npm run dev
- in order to build it for prod you'll need to run yarn build or npm run build
- Netlify is used to have a production based environment.




